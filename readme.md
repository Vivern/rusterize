Rusterize is a simple software rasterizer written entirely in rust. Given a
triangle rusterize provides an iterator that allows easy iteration over all
pixels covered by the polygon.

In addition rusterize contains convenience methods for common graphics
operations such as vertex attribute interpolation over faces.

Unlike other, similar projects rusterize doesn't lock you into a graphics
pipeline. It strictly perform the rasterization only, giving you full access to
the rust ecosystem for vertex transformations and pixel shaders.


![Cover Image](./cover_image.png "Cover Image")


# Example

The  following code iterates over all pixels covered by a triangle and prints
their position:

```rust
// Create a 2D triangle
let triangle = rusterize::Triangle {
    x1: 0.5, y1: 0.1,
    x2: 0.1, y2: 0.9,
    x3: 0.9, y3: 0.9,
};

// Find all pixels covered by the triangle
for frag in triangle.rasterize(100, 100) {
    println!("Pixel at ({}, {})", frag.x, frag.y);
}

```


# Getting started

Getting started with rusterize is easy. After installing rust download the
source code and build it with cargo as usual:

```sh
git clone git@gitlab.com:Vivern/rusterize.git
cd rusterize
cargo build
```

The `examples` directory contains well commented samples for how to use
rusterize. You can execute them via cargo:

```sh
cargo run --example triangle
```

# Status

Rusterize is in its early stages and the API should be considered unstable.
Breaking changes may occur without warning.
