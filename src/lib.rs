use std::cmp::{min, max};


#[derive(Copy, Clone)]
pub struct Triangle {
    pub x1: f32,
    pub y1: f32,

    pub x2: f32,
    pub y2: f32,

    pub x3: f32,
    pub y3: f32,
}


pub struct Fragment {
    pub x: usize,
    pub y: usize,

    pub b1: f32,
    pub b2: f32,
    pub b3: f32,
}


fn edge_function(
    ax: f32, ay: f32,
    bx: f32, by: f32,
    cx: f32, cy: f32) -> f32 {

    (cx - ax) * (by - ay) - (cy - ay) * (bx - ax)
}


fn calculate_barycentrics(px: f32, py: f32, t: &Triangle) -> (bool, f32, f32, f32) {
    // Determines whether a point lies within a triangle and - if it does -
    // computes the barycentric coordinates. The coordinates are undefined if
    // the point is not covered by the triangle.
    //
    // This function follows the method described in "A Parallel Algorithm for
    // Polygon Rasterization"

    // TODO: This value does not depend on the point and should be precomputed
    // for each triangle
    let area = edge_function(
        t.x1, t.y1,
        t.x2, t.y2,
        t.x3, t.y3,
    );

    let w0 = edge_function(
        t.x2, t.y2,
        t.x3, t.y3,
        px, py,
    );

    let w1 = edge_function(
        t.x3, t.y3,
        t.x1, t.y1,
        px, py,
    );

    let w2 = edge_function(
        t.x1, t.y1,
        t.x2, t.y2,
        px, py,
    );

    // Determine whether the triangle covers the pixel. Normally this would only
    // draw triangles with the correct winding order. Flip the condition to
    // render triangles regardless of their winding.
    let sign = area.signum();
    let is_covered = w0 * sign >= 0f32 && w1 * sign >= 0f32 && w2 * sign >= 0f32;

    // Calculate the barycentrics
    let b0 = w0 / area;
    let b1 = w1 / area;
    let b2 = w2 / area;

    (is_covered, b0, b1, b2)
}


pub struct RasterIter {
    triangle: Triangle,
    min_xi: usize,
    max_xi: usize,
    max_yi: usize,
    yi: usize,
    xi: usize,
    target_width: usize,
    target_height: usize,
}


impl Iterator for RasterIter {
    type Item = Fragment;

    fn next(&mut self) -> Option<Fragment> {
        loop {
            // Check whether rasterization is complete
            if self.yi > self.max_yi {
                return None;
            }

            // Advance the x and y indices
            let xi = self.xi;
            let yi = self.yi;

            if xi < self.max_xi {
                self.xi += 1;
            } else {
                self.xi = self.min_xi;
                self.yi += 1;
            }

            // Find the next Fragment
            let yf = (yi as f32 + 0.5) / self.target_height as f32;
            let xf = (xi as f32 + 0.5) / self.target_width as f32;

            let (is_in_triangle, b1, b2, b3) = calculate_barycentrics(xf, yf, &self.triangle);

            if is_in_triangle {
                return Some( Fragment {
                    x: xi,
                    y: yi,
                    b1: b1,
                    b2: b2,
                    b3: b3,
                } )
            }
        }
    }
}


impl Triangle {
    pub fn is_clockwise(self: &Triangle) -> bool {
        // The winding order of a convex polygon can be determined by
        // calculating the included angle between two of its sides. Triangles
        // are necessarily convex.
        //
        // Uses the simplified formula found at
        // https://en.wikipedia.org/wiki/Curve_orientation#Orientation_of_a_simple_polygon
        let determinant = (self.x2 - self.x1) * (self.y3 - self.y1) -
            (self.x3 - self.x1) * (self.y2 - self.y1);

        determinant > 0f32
    }

    pub fn rasterize(
            self: &Triangle,
            target_width: usize,
            target_height: usize) -> RasterIter {

        // Find the triangle's bounding box, in screen space
        let minx = f32::min(f32::min(self.x1, self.x2), self.x3);
        let miny = f32::min(f32::min(self.y1, self.y2), self.y3);
        let maxx = f32::max(f32::max(self.x1, self.x2), self.x3);
        let maxy = f32::max(f32::max(self.y1, self.y2), self.y3);

        // Calculate the minimum / maximum indices corresponding to the screen
        // space bounding box
        let min_xi = (minx * target_width as f32) as isize - 1;
        let min_yi = (miny * target_height as f32) as isize - 1;

        let max_xi = (maxx * target_width as f32) as isize + 1;
        let max_yi = (maxy * target_height as f32) as isize + 1;

        // Clip the bounding box to screen space [0, 1]
        // Without this Fragments outside of the image would be generated
        let max_width = (target_width - 1) as isize;
        let max_height = (target_height - 1) as isize;

        let min_xi = min(max(min_xi, 0), max_width) as usize;
        let min_yi = min(max(min_yi, 0), max_height) as usize;
        let max_xi = min(max(max_xi, 0), max_width) as usize;
        let max_yi = min(max(max_yi, 0), max_height) as usize;

        // Construct the iterator
        RasterIter {
            triangle: *self,
            min_xi: min_xi,
            max_xi: max_xi,
            max_yi: max_yi,
            yi: min_yi,
            xi: min_xi,
            target_width: target_width,
            target_height: target_height,
        }
    }
}
