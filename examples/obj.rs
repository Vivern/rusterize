use std::path::Path;
use rusterize;
use tobj;
use image;


#[allow(dead_code)]
fn shade_gradient(frag: &rusterize::Fragment) -> (f32, f32, f32) {
    (frag.b1, frag.b2, frag.b3)
}


#[allow(dead_code)]
fn shade_wireframe(frag: &rusterize::Fragment) -> (f32, f32, f32) {
    let distance = f32::min(f32::min(frag.b1, frag.b2), frag.b3);
    let col = if distance < 0.03 { 1f32 } else { 0f32 };

    (col, col, col)
}


fn draw_triangle(
    target: &mut image::RgbImage,
    triangle: &rusterize::Triangle) {

    for frag in triangle.rasterize(target.width() as usize, target.height() as usize) {
        // Shade the pixel
        let (r, g, b) = shade_gradient(&frag);
        // let (r, g, b) = shade_wireframe(&frag);

        // Apply gamma
        let gamma: f32 = 1.0 / 2.2;
        let r = r.powf(gamma);
        let g = g.powf(gamma);
        let b = b.powf(gamma);

        // Write out the pixel value
        target[(frag.x as u32, frag.y as u32)] = image::Rgb([
            (r * 255f32) as u8,
            (g * 255f32) as u8,
            (b * 255f32) as u8,
        ]);
    }
}


fn main() {
    // Create the image to render into
    let mut img: image::RgbImage = image::ImageBuffer::new(1024, 1024);

    // Load the scene
    println!("Loading scene file");

    let scene = tobj::load_obj(&Path::new("resources/suzanne.obj"));
    assert!(scene.is_ok());

    let (models, materials) = scene.unwrap();

    println!("Found {} mesh(es) and {} material(s)", models.len(), materials.len());

    // Render the meshes
    for model in models.iter() {
        let mesh = &model.mesh;
        assert!(mesh.positions.len() % 3 == 0);  // 3 coordinates per vertex
        assert!(mesh.indices.len() % 3 == 0);    // 3 vertices per face

        println!(
            "Drawing \"{}\" - {} face(s)",
            model.name,
            mesh.indices.len() / 3
        );

        for face_ii in 0..mesh.indices.len() / 3 {
            let v1_ii = mesh.indices[3 * face_ii + 0] as usize;
            let v2_ii = mesh.indices[3 * face_ii + 1] as usize;
            let v3_ii = mesh.indices[3 * face_ii + 2] as usize;

            let scale: f32 = 1.0;

            // Construct the triangle
            let triangle = rusterize::Triangle {
                x1: mesh.positions[3*v1_ii + 0] * scale,
                y1: 1.0f32 - mesh.positions[3*v1_ii + 1] * scale,

                x2: mesh.positions[3*v2_ii + 0] * scale,
                y2: 1.0f32 - mesh.positions[3*v2_ii + 1] * scale,

                x3: mesh.positions[3*v3_ii + 0] * scale,
                y3: 1.0f32 - mesh.positions[3*v3_ii + 1] * scale,
            };

            // Rasterize it
            draw_triangle(&mut img, &triangle);
        }
    }

    // Write out the result
    println!("Saving output image");
    img.save("obj_out.png").unwrap();
}
